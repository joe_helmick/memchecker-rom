; start of RAM
ramstart		= $8000
stackbottom		= $0

; serial comm and terminal output stuff
cr			= 13	; carriage return
lf			= 10	; linefeed
cs			= 12	; clear screen
bell			= 7
rts_high		= $d5
rts_low			= $95
ser_bufsize		= 63
ser_fullsize		= ser_bufsize - 10
ser_emptysize		= 5

; other constants
tst_byte		= $aa
inv_byte		= $55
stride			= 1024


defvars ramstart
{
	serbuf			ds.w 1		; the start of the serial buffer
	serinptr		ds.w ser_bufsize; the buffer's input pointer
	serrdptr		ds.w 1		; the buffer's read pointer
	serbufused		ds.w 1		; count of bytes used
	
	address			ds.w 1		; address being tested
	value			ds.b 1		; original value at address being tested
	
	
	lastvar			ds.b 1		; sentinel value
}

; ##################################################################################################
; ###########################  reset vectors #######################################################
; ##################################################################################################

	
	
hwreset:
	di			; turn off interrupts until started up
	ld	hl, serbuf	; set pointer to serial buffer
	ld	(serinptr), hl	; set tx pointer to same location
	ld	(serrdptr), hl	; set rx pointer to same location
	xor	a		; set a to zero
	ld	(serbufused), a	; set buffer used amount to zero
	ld	a, rts_low	; load the acia configuration byte...
	out	($80), a	; and initialize the acia with it
	im	1		; set interrupt mode...
	ei			; and enable interrupts
	jp	mainprogram	; jump to start of program

; ##################################################################################################
; ###########################  generic support routines ############################################
; ##################################################################################################
	
;---------------------------------------------------------------------------------------------------
; Transmit character from A register to the terminal when terminal is ready for it.
txa:
	push	af		; store char in a for later
txaloop:
	in	a, ($80)	; read acia status byte       
	bit	1, a		; set z flag if still transmitting character       
	jr	z, txaloop	; and loop until flag signals ready
	pop	af		; get the character back from the stack
	out	($81), a	; send it over rs232 to acia outpout address
	ret
	
;------------------------------------------------------------------------------------------
; Print a null-terminated string character by character wherever the cursor is.
; Address to string is in HL.
print:
	ld	a,(hl)
	or	a
	ret	z
	call	txa
	inc	hl
	jr	print
	ret
	
;---------------------------------------------------------------------------------------------------
; mfill
; From "Z80 Assembly Language Subroutines" 1983 p. 196
mfill:
	ld	(hl), a
	ld	d, h
	ld	e, l
	inc	de
	dec	bc
	ld	a, b
	or	c
	ret	z
	ldir
	ret

; ##################################################################################################
; ###########################  specific support routines and functions #############################
; ##################################################################################################

;---------------------------------------------------------------------------------------------------
 bin_to_hex:
; compute the two-character hex string representing a byte.
; input		a holds byte to convert
; output	h holds most significant nibble character
; output	l holds least significant nibble character
; uses		a, b, c, h, l
	;convert high nibble
	ld	b, a
	and	11110000b
	rrca
	rrca
	rrca
	rrca
	call	nascii
	ld	h, a
	; convert low nibble
	ld	a, b
	and	00001111b
	call	nascii
	ld	l, a
	ret
nascii:
	cp	10
	jr	c, nas1
	add	a, 7
 nas1:
	add	a, '0'
	ret


;---------------------------------------------------------------------------------------------------
print_address:
; Print a message with the current address.
; input		(address)
; output	terminal output

	push	hl
	
	ld	hl, address_msg
	call	print
	
	ld	hl, (address)
	ld	a, h
	call	bin_to_hex
	ld	a, h
	call 	txa
	ld	a, l
	call	txa
	
	ld	hl, (address)
	ld	a, l
	call	bin_to_hex
	ld	a, h
	call 	txa
	ld	a, l
	call	txa

	pop	hl
	ret

	
;###################################################################################################
;#######################     main program      #####################################################
;###################################################################################################

address_msg:		defm "addr: $", 0
isrom_msg:		defm " begins block of ROM", cr, lf, 0
isram_msg:		defm " begins block of RAM", cr, lf, 0
done_msg:		defm "done.", cr, lf, 0

mainprogram:

	; set up the stack pointer
	ld	sp, stackbottom
	
	; null out all ram variables	
	ld	hl, ramstart
	ld	bc, lastvar - ramstart
	ld	a, 0
	call	mfill
        
	; initialize registers
	ld      hl, 0
	ld	bc, stride
	
	; clear screen
	ld	a, cs
	call	txa
        
top:	
	push	bc
	call    print_address
	pop	bc
	
dotest1:
	; save value at address
	ld	a, (address)
	ld	(value), a
	
	ld	a, tst_byte
	ld	(hl), a
	cp	(hl)
	jp	z, itsram
	jp	itsrom
	
restore1:	
	; restore value
	ld	a, (value)
	ld	(address), a

increment:
	add	hl, bc		
	ld	(address), hl

testifdone:	
	ld	a, h
	cp	$fc
	jp	z, done		; done so bail out
	jr	top		; not done so repeat

done:
	ld	hl, done_msg	; print message
	call	print
	ld	a, bell		; ring bell
	call	txa
	halt
	
itsram:
	push	hl
	ld	hl, isram_msg
	call	print
	pop	hl
	jr	restore1

itsrom:	
	push	hl
	ld	hl, isrom_msg
	call	print
	pop	hl
	jr	restore1
	